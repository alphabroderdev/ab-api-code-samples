<?php
/**
 * This script can be used to test a connection to the alphabroder FTP server. 
 */


$server = 'ftp.appareldownload.com';
$port = 21;
// The folowing credentials are for the public FTP account. 
// Update FTP credentials as needed.
$username = 'A1pha';
$password = 'A1pha2oll';

// Set the connection mode to FTP_SSL for FTPS
$connection = ftp_ssl_connect($server, $port);

if (!$connection) {
    die("Failed to connect to the FTPS server.");
}



// Login to the FTP server
if (ftp_login($connection, $username, $password)) {
    echo "Connected and logged in to FTPS server successfully.";

} else {
    echo "Failed to log in to the FTPS server.";
    ftp_close($connection);
    exit;
}


// Enable passive mode
ftp_pasv($connection, true);

// Change to a specific folder on the FTPS server (replace 'path_to_folder' with the actual folder path)
$folder = '/A1pha';
if (ftp_chdir($connection, $folder)) {
    echo "Changed to folder: $folder\n";
} else {
    echo "Failed to change to folder: $folder\n";
}

echo 'pwd: ' . ftp_pwd($connection) . "\n";

// List the files in the current folder
$files = ftp_nlist($connection, '.');


if ($files === false) {
    echo "Failed to list the files in the current folder.\n";
} else {
    echo "Files in the current folder:\n";
    foreach ($files as $file) {
        echo "$file\n";
    }
}

// Close the FTP connection
ftp_close($connection);

