<?php

     // some initial environment settings
     // $destinationURL = "https://www.alphabroder.com/cgi-bin/orderUploadFile.cgi";
     $destinationURL = "https://dev.alphabroder.com/cgi-bin/orderUploadFile.cgi";
     $basicAuthUserName = "webdev";  // required for alphabroder development systems
     $basicAuthPassword = "alpdev";  // required for alphabroder development systems

     // set parameters about the order being submitted
     // please see https://www.alphabroder.com/integration for requirements about CSV file formatting and naming
     $filename = "<insert-file-name>";
     $orderCSV = file_get_contents($filename);

     // set the below parameters to what is applicable to your business
     // if you do not know your account logon or customer number, please contact edisupport@alphabroder.com
     $userName = "<logon-username>";
     $password = "<logon-password>"; 
     $customer_number = "<customer-account-number>";

     // construct the multipart message
     define('MULTIPART_BOUNDARY', '--------------------------' . microtime(true));
     $eol = "\r\n";
     $content  = "--" . MULTIPART_BOUNDARY . $eol . "Content-Disposition: form-data; name=\"FILE1\"; filename=\"" . $filename . "\"" . $eol . "Content-Type: text/html" . $eol . $eol . $orderCSV . $eol;
     $content .= "--" . MULTIPART_BOUNDARY . $eol . 'Content-Disposition: form-data; name="userName"' . $eol . $eol . $userName . $eol;
     $content .= "--" . MULTIPART_BOUNDARY . $eol . 'Content-Disposition: form-data; name="password"' . $eol . $eol . $password . $eol;
     $content .= "--" . MULTIPART_BOUNDARY . $eol . 'Content-Disposition: form-data; name="cn"' . $eol . $eol . $customer_number . $eol;
     $content .= "--" . MULTIPART_BOUNDARY . "--" . $eol;

     // make the actual HTTP POST and capture response and handle as appropriate
     $response = file_post_contents($destinationURL, $content, $basicAuthUserName, $basicAuthPassword, "Content-Type: multipart/form-data; boundary=" . MULTIPART_BOUNDARY . "\r\n");

     // obviously you'll want to do more than just echo out the response
     // this is where business logic would be implemented to record the response from Alphabroder to capture the order ID for future tracking
     echo $response;

     // helper function for constructing headers
     function file_post_contents($url, $data, $username = null, $password = null, $header)
     {
          $opts = array(
             "ssl" => array(
                 "verify_peer" => false,
                 "verify_peer_name" => false
             ),

             'http' => array(
                 'method' => 'POST',
                 'header' => $header,
                 'content' => $data,
                 'timeout' => 1200
             )
          );
          if ($username && $password) {
             $opts['http']['header'] .= ("Authorization: Basic " . base64_encode("$username:$password"));
          }
          return file_get_contents($url, false, stream_context_create($opts));
     }
?>