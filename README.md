# Alphabroder API Sample Code

## How to download
Either clone the repo or click the ... button and manually download (or just browse online and copy and paste as needed)

### About this repository
The sample code repository was designed to help jumpstart Alphabroder customers with their integration projects for the API's described at https://www.alphabroder.com/integration.  If you have any questions or comments about this repository, please reach out to edisupport@alphabroder.com.  This repository is open to the public for read-only access.  All samples are organized by technology and then by API.