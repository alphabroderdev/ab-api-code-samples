const API_BASE_URL = 'https://dev.alphabroder.com/cgi-bin/online/xml/inv-request.w?';
const API_CREDENTIALS = 'userName=<username>&password=<password>';
const API_STAGGER = 200; // number of ms between calls
const API_THREADS = 3; // number of max concurrent calls
// example set of items for which to find inventory
var items = [
    'B11007514',
    'B11007515',
    'H484P2510',
    'B007NL515',
    'B002CN513',
    'B007NL514',
    'B11007516',
    'B03307515',
    'B007NL516',
];
var itemIndex = 0;
var itemResponses = [];
var staggerIncrement = 0;
var threadCount = 0;

var main = setInterval(function(){
    try {
        if(itemResponses.length == items.length) {
            clearInterval(main);
        }
        for( ; itemIndex < items.length && threadCount < API_THREADS; itemIndex++){
            if(staggerIncrement % API_THREADS == 0) staggerIncrement=0; // reset stagger each time we make API_THREADS requests
            
            console.log('calling '+items[itemIndex]+' with '+staggerIncrement*API_STAGGER + ' ms stagger');
            
            getInventory(items[itemIndex], staggerIncrement)
            .then(response=>{
                itemResponses.push(response);
            })
            .catch(error => {
                itemResponses.push(error);
                // handle errors here
            })
            .finally(() => {
                threadCount--;
            });
          staggerIncrement++;
          threadCount++;
        }
      } catch(error) {
        console.log("Error On Try getInventory: "+error);
      }
    
}, 1);


const getInventory = (itemId, stagger) => {
    return new Promise((resolve,reject) => {
      setTimeout(function(){
        let APIURL = API_BASE_URL + API_CREDENTIALS + '&in1=' + itemId + '&pr=y';
        const axios = require('axios')
        axios.get(APIURL, {
          headers: {
            'Authorization': 'Basic ' + Buffer.from('webdev:alpdev').toString("base64") // this is needed for dev
          }
        })
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
      }, stagger*API_STAGGER );  // this forces some spacing out of the requests
    })
  }  