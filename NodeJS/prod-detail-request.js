// Set sr, userName, and password for your request.
let data = {
    'sr' : '<sr>',
    'userName' : '<userName>',
    'password': '<password'
};
let outer_options = {
    host: 'dev.alphabroder.com',
    port: 443,
    path: '/cgi-bin/online/xml/prod-detail-request.w?sr='+data.sr+'&userName='+data.userName+'&password='+data.password,
    method : 'GET',
    headers: {
        // This Authorization field is only required for AlphaBroder development systems.
        'Authorization': 'Basic ' + Buffer.from('webdev:alpdev').toString("base64")
    }
};

return new Promise((resolve, reject) =>
{
    let lib = require('https');
    const request = lib.request(outer_options, (response) => {
        if (response.statusCode < 200 || response.statusCode > 299) {
            reject(new Error('Failed to load page,status code: ' + response.statusCode));
        }
        response.on('data', (data) => {
            resolve(data.toString('utf8'));
        });
    });
    request.end();
    request.on('error', (err) => reject(err));
})
.then((data) => console.log(data))
.catch((err) => console.log(err));

  
